import random
import os
import shutil
import sys
import time
import traceback
import inspect
import bdb
import cmd
import linecache

WIDTH = 120
HEIGHT = 50
WIDTH, HEIGHT = shutil.get_terminal_size()
HEIGHT -= 2 # Safety buffer

DIVIDER = " | "
SIDE_WIDTH = int(WIDTH * 0.25)
MAIN_WIDTH = WIDTH - (SIDE_WIDTH + len(DIVIDER))

def center_in_width(text, width, filler=" "):
    margins = (width - len(text)) // 2
    assert margins >= 0
    centered = (margins * filler) + text + (margins * filler)
    if len(centered) < width:
        centered += filler*(width - len(centered))
    return centered


LOCALS_TITLE = center_in_width("LOCALS", SIDE_WIDTH)
TB_TITLE = center_in_width("Traceback", MAIN_WIDTH)

def random_filename():
    bank = ["special", "important", "awesome", "bad", "ugly", "boring", "exciting"]
    one = bank[random.randint(0, len(bank)-1)]
    two = bank[random.randint(0, len(bank)-1)]
    return os.path.join("my", one, two, "file.py")

def expand_to_width(text, width, extra=" "):
    return text + ((width - len(text)) * extra)

def elide_to_width(text, width):
    elide = "..."
    return text[:width-len(elide)] + elide

def expand_or_elide(text, width):
    if len(text) == width:
        return text
    if len(text) < width:
        return expand_to_width(text, width)
    else:
        return elide_to_width(text, width)

def iter_locals_table(dlocals):
    table_width = SIDE_WIDTH
    # dlocals = locals()
    # dlocals = os.environ
    out = 0
    for key, val in dlocals.items():
        if out == HEIGHT:
            raise StopIteration
        line = key + "=" + repr(val)
        yield expand_or_elide(line, table_width)
        out += 1

def iter_tb_view(frame):
    tb_width = MAIN_WIDTH
    stack_summary = traceback.extract_stack(frame)
    tb_depth = 0
    for frame_summary in stack_summary:
        leading_arrow = "    " * tb_depth #  + "  "
        file_prefix = leading_arrow + str(frame_summary.lineno) + ": " 
        line = file_prefix + frame_summary.filename
        yield expand_or_elide(line, tb_width)
        indent_size = len(file_prefix)
        yield expand_or_elide(indent_size * " "  + frame_summary.line, tb_width)
        tb_depth += 1


PROMPT = ">>>"

def _prep_and_show(frame):
    intro = worse_gui(frame)[:-5]
    WorsePdb(frame).cmdloop(intro=intro)

def worse_gui(frame, feedback=""):
    fake_output = ""
    def print(next_line):
        nonlocal fake_output
        fake_output += next_line + "\n"

    dlocals = frame.f_locals.copy()
    print(center_in_width("Welcome to worse pdb", WIDTH, filler="-"))
    # print("_" * WIDTH)
    print(LOCALS_TITLE + DIVIDER + TB_TITLE)
    print("-" * WIDTH)
    locals_lines = list(iter_locals_table(dlocals))
    tb_lines = list(iter_tb_view(frame))
    tb_loc_height = max([len(locals_lines), len(tb_lines), int(HEIGHT * 0.3)])
    for i in range(tb_loc_height):
        #print(i)
        #print(locals_lines)
        #print(tb_lines)
        try:
            locals_line = locals_lines[i]
        except IndexError:
            locals_line = expand_or_elide(" ", SIDE_WIDTH)
        try:
            tb_line = tb_lines[i]
        except IndexError:
            tb_line = expand_or_elide(" ", MAIN_WIDTH)
        print(locals_line + DIVIDER + tb_line)
    # print("-"*WIDTH)
    frame_summary = traceback.extract_stack(frame, limit=1)[0]
    print(center_in_width(frame_summary.filename, WIDTH, filler="-"))
    # print(expand_or_elide(frame_summary.filename, WIDTH))
    frame_info = inspect.getframeinfo(frame)
    remaining = HEIGHT - 5 - tb_loc_height
    if frame_summary.lineno < (remaining // 2):
        line_offset = (frame_summary.lineno * -1) + 1
    else:
        line_offset = 0 - (remaining // 2)
    for _ in range(remaining):
        lineno = frame_summary.lineno + line_offset
        line_offset += 1
        # Trim off last char in line, it's gonna be a newline.
        line = str(lineno).zfill(2) + "|"
        line += linecache.getline(frame_summary.filename, lineno)[:-1]
        if lineno == frame_summary.lineno:
            line += " <---"
        print(expand_or_elide(line, WIDTH))
        if lineno == frame_summary.lineno:
            # print("="*frame_info.index)
            pass
    # raise ValueError(dir(frame_summary))
    # print(expand_or_elide((" "*(frame_info.index-1)) + "^", WIDTH))
    print("-"*WIDTH)
    print(expand_or_elide(feedback, WIDTH))
    print(PROMPT)
    return fake_output

class WorsePdb(cmd.Cmd, bdb.Bdb):
    def __init__(self, frame):
        bdb.Bdb.__init__(self)
        cmd.Cmd.__init__(self)
        # This is for sure wrong, just trying to make
        # things appear
        self.prompt = ">>>"
        self.bottom_frame = frame
        self.frames = [frame]
        self.frame = frame
        self.feedback = ""
        self.set_trace(frame)

    def postcmd(self, stop, response):
        if stop:
            return True
        self.prompt = worse_gui(self.frame, feedback=self.feedback)[:-1]
        return False

    def default(self, response):
        d_name = "<worse pdb>"
        self.feedback = ""
        eval_problem = None
        try:
            resp_code = compile(response, d_name, "eval")
            self.feedback = repr(eval(
                resp_code, self.frame.f_globals, self.frame.f_locals
            ))
        except Exception as e:
            eval_problem = e
            self.feedback = "'" + response + "'"
            self.feedback += " " + str(eval_problem)
        if eval_problem:
            try:
                resp_code = compile(response, d_name, "exec")
                self.feedback = repr(exec(
                    resp_code, self.frame.f_globals, self.frame.f_locals
                ))
            except Exception as exec_problem:
                self.feedback += " & " + str(exec_problem)
        self.postcmd(False, response)

    def emptyline(self):
        # Do nothing, default is to redo
        return

    def do_quit(self, line): return True
    do_exit = do_quit

    def do_up(self, line):
        if not self.frame.f_back:
            self.feedback = "At the top, can't go up!"
            return
        self.frame = self.frame.f_back
        if self.frame not in self.frames:
            self.frames.append(self.frame)
        self.feedback = "Up one stack frame"

    def do_down(self, line):
        if self.frame == self.bottom_frame:
            self.feedback = "At the bottom, can't go down! Maybe you want to 'step'?"
            return
        cur_idx = self.frames.index(self.frame)
        self.frame = self.frames[cur_idx - 1]
        self.feedback = "Down one stack frame"

    def do_step(self):
        self.set_step()

def debug(frame=None):
    if frame == None:
        frame = sys._getframe().f_back
    _prep_and_show(frame)

"""Would be cool to serialize a StackSummary and be able to load it back and inspect locals at each frame."""
